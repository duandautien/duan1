<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;   
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'=>'required',
            'age'=>'required',
            'national'=>'required',
            'position'=>'required',
            'salary'=>'required',
            'avarta'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'required'=>'Hãy nhập :attribute',
        ];
    }
    public function attributes(){
        return [
            'name'=>'tên',
            'age'=>'tuổi',
            'national'=>'quốc tịch',
            'position'=>'vị trí',
            'salary'=>'lương',
            'avarta'=>'avarta',
        ];
    }
}
