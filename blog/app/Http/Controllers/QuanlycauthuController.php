<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBlogPost;
use App\Qlct; 

class QuanlycauthuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Qlct::all();
        return view("qlct/index",compact("data")); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view ("qlct/add");    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogPost $request)
    { 
            $Qlct=new Qlct();
                $Qlct->name=$request->name;
                $Qlct->age=$request->age;
                $Qlct->national=$request->national;
                $Qlct->position=$request->position;
                $Qlct->salary=$request->salary;
                $Qlct->avarta=$request->avarta;
                $Qlct->save();
            return view("qlct/thongbao"); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Qlct::where('id',$id)->get();
        return view("qlct/edit",compact("data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBlogPost $request, $id)
    {
           $Qlct=Qlct::where('id',$id)->update([  
                'name'=>$request->name,
                'age'=>$request->age,
                'national'=>$request->national,
                'position'=>$request->position,
                'salary'=>$request->salary,
                'avarta'=>$request->avarta]);
            return view("qlct/thongbao");   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Qlct= Qlct::find($id);
        $Qlct->delete();
        return view("qlct/thongbao"); 
    }
}
