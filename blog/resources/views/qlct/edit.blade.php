<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
        $name="";
        $age="";
        $national="";
        $position="";
        $salary="";
        $avarta="";
        foreach ($data as $key => $value)
        {
             $name=$value->name;
             $age=$value->age;
             $national=$value->national;
             $position=$value->position;
             $salary=$value->salary;
             $avarta=$value->avarta;
        } 
        ?>
        <form action="" method="post">
        {{ csrf_field() }}
        <th>Tên cầu thủ</th>
        <input type="text" name="name" value='<?php echo $name; ?>'>
        <th>Tuổi</th>
        <input type="text" name="age" value='<?php echo $age; ?>'> 
        <th>Quốc tịch</th>
        <input type="text" name="national" value='<?php echo $national; ?>'>
        <th>Vị trí</th>
        <input type="text" name="position" value='<?php echo $position; ?>'>
        <th>Lương</th>
        <input type="text" name="salary" value='<?php echo $salary; ?>'>
        <p>Avartar</p>
        <input type="file" name="avarta" value=''>
        <button name="submit" type="submit">Cập Nhật Cầu Thủ</button>
    </form>
       @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $errors)
                        <li>{{$errors}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </body>
</html>

