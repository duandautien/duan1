<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/blog', function () {
    return view('blog');
});
Route::get('/blog/detail', function () {
    return view('blog-detail');
});
Route::get('/qlct/index',"QuanlycauthuController@index");
Route::get('/qlct/add',"QuanlycauthuController@create");
Route::post('qlct/add',"QuanlycauthuController@store");
Route::get('/qlct/edit/{id}',"QuanlycauthuController@edit");
Route::post('qlct/edit/{id}',"QuanlycauthuController@update");
Route::get('/qlct/delete/{id}',"QuanlycauthuController@destroy");






